# Democats

Democats website

## Install Project dependencies

```
$ pip install sqlalchemy_utils sqlalchemy flask --user
```

## Python server

1. Open runserver.py
2. Open localhost:5555

## Gulp

1. Install node modules

```
$ npm i
```

2. Start using gulp

```
$ gulp
```
