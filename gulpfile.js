
const gulp                      = require('gulp'),
      sourcemaps                = require('gulp-sourcemaps'),
      plumber                   = require('gulp-plumber'),
      sass                      = require('gulp-sass'),
      autoprefixer              = require('gulp-autoprefixer'),
      cssnano                   = require('gulp-cssnano'),
      babel                     = require('gulp-babel'),
      uglify                    = require('gulp-uglify'),
      concat                    = require('gulp-concat'),
      browserSync               = require('browser-sync').create(),

      src_folder                = './src/',
      dist_folder               = './poc/static/',
      node_modules_folder       = './node_modules/'



gulp.task('html', () => {
  return gulp.src([ dist_folder + '**/*.html' ])
});

gulp.task('sass', () => {
  return gulp.src([ src_folder + 'sass/main.sass' ])
    .pipe(sourcemaps.init())
      .pipe(plumber())
      .pipe(sass())
      .pipe(autoprefixer({
        browsers: [ 'last 3 versions', '> 0.5%' ]
      }))
      .pipe(cssnano())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dist_folder + 'css'))
});

gulp.task('js', () => {
  return gulp.src([ src_folder + 'js/**/*.js' ])
    .pipe(plumber())
    .pipe(sourcemaps.init())
      .pipe(babel({
        presets: [ 'env' ]
      }))
      .pipe(concat('main.min.js'))
      .pipe(uglify())
    .pipe(sourcemaps.write('.'))
    .pipe(gulp.dest(dist_folder + 'js'))
});

gulp.task('vendorJs', () => {
  return gulp.src([ 
    // node_modules_folder + 'bootstrap/dist/js/bootstrap.min.js'
    ])
    .pipe(plumber())
    .pipe(concat('vendor.min.js'))
    .pipe(gulp.dest(dist_folder + 'js'))
});



gulp.task('build', gulp.series('html', 'sass', 'js'));

gulp.task('serve', () => {
  return browserSync.init({
    server: {
      baseDir: [ 'shrug' ],
      port: 3000
    },
    open: false
  });
});

gulp.task('watch', () => {
  let watch = [
    dist_folder + '**/*.html',
    src_folder + 'sass/**/*.sass',
    src_folder + 'sass/**/*.scss',
    src_folder + 'js/**/*.js'
  ];

  gulp.watch(watch, gulp.series('build')).on('change', browserSync.reload);
});

gulp.task('default', gulp.series('build', gulp.parallel('serve', 'watch')));
