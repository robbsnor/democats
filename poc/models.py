"""
Data definition for the application.
"""
from sqlalchemy import Column, ForeignKey, Integer, String, Float, DateTime, Boolean
from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy_utils import create_database, database_exists
from datetime import datetime

Basis = declarative_base()

# create table gebruikers, voeg meer info toe zodien nodig


class User(Basis):
    __tablename__ = 'users'
    id = Column(Integer, primary_key=True)
    username = Column(String(40), nullable=False)
    password = Column(String(10), nullable=False)
    creation_date = Column(DateTime, nullable=False)

    def __init__(self, Username, Password,  Creation_date):
        self.username = Username
        self.password = Password
        self.creation_date = Creation_date


# Create a database manager for a MySQL or MariaDB instance
engine = create_engine("mysql+pymysql://root:kaas@localhost:3306/webapp")

# USE DATABASE DROP ONLY FOR DEVELOPMENT PURPOSES
# if database_exists(engine.url):
#  drop_database(engine.url)

# Create a new, empty database, if one does not exist
if not database_exists(engine.url):
    create_database(engine.url)

# Create the tables. In SQL, this would be equal to using
# the CREATE TABLE instruction
Basis.metadata.create_all(engine)
