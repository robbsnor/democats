"""
Routes and views for the flask application.
"""

from datetime import datetime
from flask import Flask, request, render_template
from poc import app
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from poc.models import Basis
from datetime import datetime
from poc.models import User
import time


@app.route('/')
@app.route('/home')
def home():
    """Renders the home page."""
    return render_template(
        'register.html',
        title='Home',
        year=datetime.now().year,
    )

@app.route('/profile')
def profile():
    return render_template(
        'profile.html',
        title='Home',
        year=datetime.now().year,
    )


@app.route('/register')
def register():
    return render_template(
        "register.html"
    )


@app.route('/registering', methods=['POST'])
def registering():
    print("registering")
    username = request.form['Username']
    password = request.form['Password']
    passwordRepeat = request.form['Password_repeat']
    creation_date = datetime.now
    DBSession = sessionmaker(bind=engine)
    session = DBSession()
    new_user = User(Username=username, Password=password, Creation_date=creation_date)
    session.add(new_user)
    session.commit()


@app.route('/login')
def login():
    return render_template(
        "login.html"
    )

###################################################################
engine = create_engine("mysql+pymysql://root:kaas@localhost:3306/webapp")
Basis.metadata.bind = engine
DBSession = sessionmaker(bind=engine)
