"""
This script runs the Hairforce One Proof-of-Concept application using a development server.
"""

from os import environ
from poc import app

if __name__ == '__main__':
    HOST = environ.get('SERVER_HOST', '0.0.0.0')
    try:
        PORT = int(environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 8001
    app.run(HOST, PORT)
